// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;
pragma experimental ABIEncoderV2;

import "./ESToken.sol";

/**
 * Author: Nan Wang
 * Date: 22-01-2021
 **/
contract MultiSig {

    event Deposit(address indexed sender, uint amount, uint balance);
    event SubmitTransaction(
       address indexed submitter,
       uint indexed txKey,
       uint256 total,
       int256 zr,
       uint256 cprime,
       uint256 cpayment
    );

    event ConfirmTransaction(address indexed owner, uint indexed txKey, ZkpNN updatedZkp);
    event ExecuteTransaction(address indexed owner, uint indexed txKey);
    event RevokeConfirmation(address indexed owner, uint indexed txKey);

    address private operator;
    mapping(uint => address[]) private users;

    mapping(uint => mapping(address => bool)) private isConfirmed;

    mapping(uint => bool) private transactionKeys;
    mapping(uint => mapping(address => uint256)) private txCprimes;
    mapping(uint => mapping(address => uint256)) private txCpayments;
    mapping(uint => Transaction) public transactions;

    ESToken private token;

    Pedersen private pdr;

    struct Transaction {
        uint256 total;
        int256 zr;
        bool executed;
        uint numConfirmations;
    }

    struct ZkpSumRequest {
        uint256 total;
        int256 zr;
        uint256 cprime;
        uint256 cpayment;
    }

    modifier onlyOperator() {
        require (
            msg.sender == operator,
            "Only operator can call this."
        );
       _;
    }

    modifier onlyRegisteredUser(uint _txKey){
        require (
            Utils.addressIsIn(msg.sender, users[_txKey]),
            "Only allowed user can call this."
        );
       _;
    }

    modifier txExists(uint _txKey) {
        require (
            transactionKeys[_txKey],
            "tx does not exist"
        );
        _;
    }

    modifier notConfirmed(uint _txKey) {
        require (
            !isConfirmed[_txKey][msg.sender],
            "tx already confirmed"
        );
        _;
    }

    modifier notExecuted(uint _txKey) {
        require(
            !transactions[_txKey].executed,
            "tx already executed"
        );
        _;
    }

    constructor(address _operator, address payable _tokenAddr, address _pedersenAddr) {
        operator = _operator;
        token = ESToken(_tokenAddr);
        pdr = Pedersen(_pedersenAddr);
    }

    /*
     * Each registered user for a certain transaction
     * with _txKey, can submit a zkpsum
     **/
    function submitTransaction(uint _txKey,
                               ZkpSumRequest memory req)
        public
        onlyRegisteredUser(_txKey)
    {
        // create a new transaction if _txkey does not exist
        if (!transactionKeys[_txKey]) {
            transactionKeys[_txKey] = true;
            transactions[_txKey] = Transaction(req.total,
                                        req.zr, false, 0);
        }
        //obtain the current transaction with _txKey
        Transaction storage transaction = transactions[_txKey];
        //check whether the total payment and zr in the zkpsum
        //are equal to the stored ones
        require(transaction.total == req.total);
        require(transaction.zr == req.zr);
        //store the cprime and cpayment of the calling user
        txCprimes[_txKey][msg.sender] = req.cprime;
        txCpayments[_txKey][msg.sender] = req.cpayment;
        //emit the event of SubmitTransaction
        emit SubmitTransaction(msg.sender, _txKey, req.total,
                               req.zr, req.cprime, req.cpayment);
    }

    /**
     * Each registered user can confirm the transaction with
     * _txkey in the condition that this transaction exists,
     * not confirmed by the calling user, or executed
     **/
    function confirmTransaction(uint256 _txKey,
                                ZkpNN calldata updatedZkp)
        public
        onlyRegisteredUser(_txKey)
        txExists(_txKey)
        notConfirmed(_txKey)
        notExecuted(_txKey)
    {
        //obtain the transaction with _txkey
        Transaction storage transaction = transactions[_txKey];
        //increment the number of confirmation by 1
        transaction.numConfirmations += 1;
        //enable the confirm flag of the calling user
        isConfirmed[_txKey][msg.sender] = true;
        //verify the non-negative zero-knowledge proof and
        //approve the transfer to operator
        token.approveTransfer(operator,
                              txCpayments[_txKey][msg.sender],
                              updatedZkp);
        //emit the ConfirmTransaction event
        emit ConfirmTransaction(msg.sender, _txKey, updatedZkp);
    }

    /**
     * The operator can execute the transaction after all the
     * registered users have confirmed it in the condition that
     * the transaction exists and not executed
     **/
    function executeTransaction(uint256 _txKey)
        public
        onlyOperator
        txExists(_txKey)
        notExecuted(_txKey)
    {
        //obtain the transaction
        Transaction storage transaction = transactions[_txKey];
        //check whether the required confirmations are made
        uint size = users[_txKey].length;
        require(transaction.numConfirmations == size,
                "the number of confirmations is not attained");
        //enable the execute flag
        transaction.executed = true;
        //group the data in a memory array
        uint256[] memory cprimes = new uint256[](size);
        uint256[] memory cpayments = new uint256[](size);
        for(uint i=0;i<size;i++){
            address _user = users[_txKey][i];
            cprimes[i] = txCprimes[_txKey][_user];
            cpayments[i] = txCpayments[_txKey][_user];
        }
        //verify the sum zero-knowledge proof and execute
        token.secretlyJointTransfer(
            users[_txKey], operator,
            ZkpSum(transaction.total, transaction.zr,
                   cprimes,cpayments)
            );
        //emit the ExecuteTransaction event
        emit ExecuteTransaction(msg.sender, _txKey);
    }

    /**
     * Each user can revoke the confirmation of the transaction
     **/
    function revokeConfirmation(uint _txKey)
        public
        onlyRegisteredUser(_txKey)
        txExists(_txKey)
        notExecuted(_txKey)
    {
        Transaction storage transaction = transactions[_txKey];

        require(isConfirmed[_txKey][msg.sender], "tx not confirmed");

        transaction.numConfirmations -= 1;
        isConfirmed[_txKey][msg.sender] = false;

        token.disapproveTransfer(operator);

        emit RevokeConfirmation(msg.sender, _txKey);
    }

    /**
     *  Register users for a transaction
     **/
    function registerUsers(uint _txKey, address[] memory _users)
        public
        onlyOperator
    {
        require (_users.length > 0, "the array of users is empty!");

        require (users[_txKey].length == 0, "the users for one transaction can only be initialized once!");

        for(uint i=0;i<_users.length;i++) {
            require (!Utils.addressIsIn(_users[i], users[_txKey]), "this user has already been registered");
            users[_txKey].push(_users[i]);
        }

        token.initFreeTokens(_users, 100);
    }

    function getNoOfUsers(uint _txKey)
        public
        view
        returns (uint)
    {
       return users[_txKey].length;
    }

    function getNoOfConfirmations(uint _txKey)
        public
        view
        txExists(_txKey)
        returns (uint)
    {
       return transactions[_txKey].numConfirmations;
    }

    function TxKeyExists(uint _txKey)
        public
        view
        returns (bool)
    {
        return transactionKeys[_txKey];
    }

    /**
     * Default fallback function
     **/
    receive() payable external {
        emit Deposit(msg.sender, msg.value, address(this).balance);
    }
}
