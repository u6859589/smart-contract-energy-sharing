// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;
pragma experimental ABIEncoderV2;

import "./Pedersen.sol";

/**
 * Author: Nan Wang
 * Date: 12-01-2021
 **/
contract ESToken {

    event Deposit(address indexed sender, uint amount, uint balance);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    event Disapproval(address indexed _owner, address indexed _spender);

    string private name = "ESToken";

    string private symbol = "es";

    uint8 private decimals;

    uint256 private totalSupply;

    mapping(address => uint256) private balances; // address -> pedersen commitment

    mapping(address => mapping(address => uint256)) private allowed; // address -> (address -> pedersen commitment)

    address payable private owner;

    Pedersen private pdr;

    modifier onlyOwner() {
        require(
            msg.sender == owner,
            "Only owner can call this."
            );
        _;
    }

    /**
     * init operator as the contract owner
     * param (g, h, q) -> the parameters of Pedersen
     *
     */
    constructor(uint256 _totalSupply, address _pdrAddr) {
        owner = payable(msg.sender);
        totalSupply = _totalSupply;
        pdr = Pedersen(_pdrAddr);
        balances[owner] = pdr.commitToWithNoRand(_totalSupply);
    }

    /**
     * A user wants to buy some tokens with ethers from the owner
     * param zkpNNOfOwner -> the non-negative zkp of the owner proving its updated balance is non-negative
     **/
    function buyTokenFrom(address seller, ZkpNN calldata zkpNNOfSeller)
        external
        payable
    {
        require(msg.value > 0, "ether must be positive");

        pdr.verifyZkpNN(zkpNNOfSeller);

        uint256 tokens = pdr.commitToWithNoRand(msg.value);
        uint256 updatedBal = getSubBalance(seller, tokens);

        require (updatedBal == zkpNNOfSeller.c);

        balances[msg.sender] = pdr.homoAdd(balances[msg.sender], tokens);
        balances[seller] = updatedBal;
    }

    /**
     * A user wants to exchange some tokens for ethers from the contract owner
     * param _value -> the amount of ethers to exchange
     * param zkpNNOfRequester -> the non-negative zkp of the updated balance of the requester
     **/
    function burnToken(uint256 _value, ZkpNN calldata zkpNNOfRequester)
        external
        payable
    {
        require(address(this).balance >= _value, "the balance of the contract is not sufficient");

        pdr.verifyZkpNN(zkpNNOfRequester);

        uint256 tokens = pdr.commitToWithNoRand(_value);

        uint256 updatedBal = getSubBalance(msg.sender, tokens);
        require(updatedBal == zkpNNOfRequester.c, "the updated balance is not equal to the received one");

        balances[msg.sender] = updatedBal;
        balances[owner] = pdr.homoAdd(balances[owner], tokens);

        // transfer the value amount of ether to the sender
        (bool success, ) = msg.sender.call{value : _value}('');
        assert (success);
    }

    function approveTransfer(address recipient, uint256 _value, ZkpNN memory zkpNN)
        public
    {
        pdr.verifyZkpNN(zkpNN);
        uint256 updatedValue = getSubBalance(tx.origin, _value);
        require(updatedValue == zkpNN.c, "the updated balance is not equal to the received one");

        allowed[tx.origin][recipient] = _value;

        emit Approval(tx.origin, recipient, _value);
    }

    function disapproveTransfer(address recipient)
        public
    {
        allowed[tx.origin][recipient] = 1;

        emit Disapproval(tx.origin, recipient);
    }

    /*
     * Invoked by the ExecuteTransaction function of the
     * MultiSignature smart contract
     **/
    function secretlyJointTransfer(address[] memory senders,
                    address recipient, ZkpSum calldata zkpsum)
        public
    {
        //verify the sum zkp
        pdr.verifyZkpSum(zkpsum);
        for(uint i=0;i<senders.length;i++) {
            address sender = senders[i];
            //check whether the paymant has been approved
            require (allowed[sender][recipient]
                                == zkpsum.cpayments[i]);
            //debit the encrypted payment in Pedersen commitment
            //from each user's account
            balances[sender] = pdr.homoSub(balances[sender],
                                        zkpsum.cpayments[i]);
            //reset the allowed payment to zero (g^0=1)
            allowed[sender][recipient] = 1;
        }
        //credit the encrypted total cost to operator's account
        balances[recipient] = pdr.homoAdd(balances[recipient],
                         pdr.commitToWithNoRand(zkpsum.cost));
    }

    /**
     * Receive tokens from the sender
     * param zkpNNOfSender -> the non-negative zkp of the updated balance of the sender
     **/
    function receiveFrom(address sender, uint256 _value, ZkpNN calldata zkpNNOfSender)
        public
    {
        pdr.verifyZkpNN(zkpNNOfSender);

        uint256 updatedBal = getSubBalance(sender, _value);
        require(updatedBal == zkpNNOfSender.c);

        balances[sender] = updatedBal;
        balances[msg.sender] = pdr.homoAdd(balances[msg.sender], _value);

        emit Transfer(sender, msg.sender, _value);
    }

    /**
     * The owner can withdraw the ethers from the contract
     **/
    function withdrawEther(uint256 _value)
        public
        payable
        onlyOwner
    {
        if(address(this).balance >= _value) {
            // transfer the value amount of ether to the ower
            (bool success, ) = owner.call{value : _value}('');
            assert (success);
        }
    }

    /**
     * Get the balance of the account
     **/
    function getBalance(address _account)
        public
        view
        returns (uint256)
    {
        return balances[_account];
    }

    function getSubBalance(address _account, uint256 _value)
        private
        view
        returns (uint256)
    {
        return pdr.homoSub(balances[_account], _value);
    }

    function initFreeTokens(address[] memory _users, uint256 value)
        public
    {
       uint size = _users.length;
       uint256 tokens = pdr.commitToWithNoRand(value);
       for(uint i=0;i<size;i++) {
           balances[_users[i]] = tokens;
       }
       balances[owner] = pdr.homoSub(balances[owner], pdr.commitToWithNoRand(size * value));
    }

    /**
     * Fallback function
     **/
    receive() payable external {
        emit Deposit(msg.sender, msg.value, address(this).balance);
    }
}
