// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;
pragma experimental ABIEncoderV2;

import "./Utils.sol";

/**
 * Author: Nan Wang
 * Date: 12-01-2021
 **/

struct ZkpNN {
    uint256 c;
    uint256 cprime;
    int256 zr;
    ZkpMbs[] mbs;
}

struct ZkpSum {
    uint256 cost;
    int256 zr;
    uint256[] cprimes;
    uint256[] cpayments;
}

struct ZkpMbs {
    uint256 c;
    uint256[] cprimes;
    int256[] zxs;
    int256[] zrs;
    uint256[] xs;
    int256[] betas;
}

contract Pedersen {

    using SafeMath for uint256;
    using SafeMath for int256;

    uint256 private g;

    uint256 private h;

    uint256 private q;

    uint256 private k;

    constructor(uint256 _g, uint256 _h, uint256 _q, uint256 _k) {
        g = _g;
        h = _h;
        q = _q;
        k = _k;
    }

    function commitToWithNoRand(uint256 a)
        public
        view
        returns (uint256)
    {
        return g.modPow(a, q);
    }

    function commitTo(uint256 v, uint256 r)
        public
        view
        returns (uint256)
    {
        return mulmod(g.modPow(v, q), h.modPow(r, q), q);
    }

    function homoAdd(uint256 c1, uint256 c2)
        public
        view
        returns (uint256)
    {
        return mulmod(c1, c2, q);
    }

    function homoAdd(uint256[] memory cs)
        public
        view
        returns (uint256)
    {
        uint256 sum = 1;
        for(uint i=0;i<cs.length;i++){
            sum = mulmod(sum, cs[i], q);
        }

        return sum.mod(q);
    }

    function homoSub(uint256 c1, uint256 c2)
        public
        view
        returns (uint256)
    {
        int256 neg = -1;
        return mulmod(c1, homoMul(c2, neg), q);
    }

    function homoMul(uint256 c, uint256 e)
        public
        view
        returns (uint256)
    {
        return c.modPow(e, q);
    }

    function homoMul(uint256 c, int256 e)
        public
        view
        returns (uint256)
    {
        return c.modPow(e, q);
    }

    function verifyZkpSum(ZkpSum memory zkp)
        public
        view
    {
        require(zkp.cost >= 0);
        require(zkp.cprimes.length > 0);
        require(zkp.cpayments.length > 0);

        uint256 cprime = homoAdd(zkp.cprimes);

        uint256 beta = uint256(keccak256(abi.encodePacked(cprime))).mod(k);

        uint256 ret1 = mulmod(g.modPow(zkp.cost.mul(beta), q), h.modPow(zkp.zr, q), q);

        uint256 ret2 = cprime;
        for(uint i=0;i<zkp.cpayments.length;i++){
            ret2 = homoAdd(ret2, homoMul(zkp.cpayments[i], beta));
        }
        ret2 = ret2.mod(q);

        assert (ret1 == ret2);
    }

    function verifyZkpNN(ZkpNN memory zkp)
        public
        view
    {
        require(zkp.mbs.length > 0);

        uint256[] memory cs = new uint256[](zkp.mbs.length);
        for(uint i=0;i<zkp.mbs.length;i++){
            verifyZkpMbs(zkp.mbs[i]);
            cs[i] = zkp.mbs[i].c;
        }

        uint256 beta = uint256(keccak256(abi.encodePacked(cs, zkp.c, zkp.cprime))).mod(k);

        uint256 ret1 = h.modPow(zkp.zr, q);

        uint256 base = 2;
        uint256 ret2 = homoAdd(zkp.cprime, homoMul(zkp.c, beta.negate()));
        for(uint i=0;i<cs.length;i++){
           ret2 = homoAdd(ret2, cs[i].modPow(beta.mul(base.pow(i)), q));
        }
        ret2 = ret2.mod(q);

        assert (ret1 == ret2);
    }

    function verifyZkpMbs(ZkpMbs memory zkp)
        public
        view
    {
        require(zkp.cprimes.length > 0);
        require(zkp.betas.length == zkp.cprimes.length);
        require(zkp.zxs.length == zkp.cprimes.length);
        require(zkp.zrs.length == zkp.cprimes.length);
        require(zkp.xs.length == zkp.cprimes.length);

        uint256 beta = uint256(keccak256(abi.encodePacked(zkp.cprimes, zkp.c))).mod(k);

        require(beta == uint256(Utils.sumOf(zkp.betas)));

        for(uint i=0;i<zkp.xs.length;i++){
            uint256 ret1 = mulmod(g.modPow(zkp.zxs[i], q), h.modPow(zkp.zrs[i], q), q);

            int256 tmp = zkp.xs[i].mul(zkp.betas[i]);
            uint256 ret2 = mulmod(homoAdd(zkp.cprimes[i], homoMul(zkp.c, zkp.betas[i])), g.modPow(tmp.negate(), q), q);

            assert (ret1 == ret2);
        }
    }
}
